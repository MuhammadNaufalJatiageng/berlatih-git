@extends('adminlte.master')

@section('title')
Halaman Form
@endsection

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Form Sign Up</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>Firstname :
            <br><br>
            <input type="text" name="namadepan">
        </label>
        <br><br>
        <label>last name :
            <br><br>
            <input type="text" name="namabelakang">
        </label><br><br>
        <p>Gender</p>
        <input type="radio" name="gender" id="male"><label for="male">Male</label>
        <br><input type="radio" name="gender" id="female"><label for="female">Female</label><br><br>
        <label for="">Nationality<br><br>
            <select name="" id="">
                <option value="Indonesia">Indonesia</option>
                <option value="Amerika">Amerika</option>
                <option value="Inggris">Inggris</option>
            </select>
        </label><br><br>
        <label for="">Language Spoken<br><br>
            <input type="checkbox" name="language">Bahasa Indonesia<br>
            <input type="checkbox" name="language">English<br>
            <input type="checkbox" name="language">Other
        </label><br><br>
        <label for="">Bio<br><br>
            <textarea name="bio" cols="40" rows="10"></textarea><br>
            <button type="submit" value="submit" >Sign Up</button>
        </label>
    </form>
</body>
</html>
@endsection