@extends('adminlte.master')

@section('title')
Halaman Index
@endsection

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Welcome!!</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <h1>SELAMAT DATANG! {{$namadepan." ".$namabelakang}}</h1>
    <h4>Terima kasih telah bergabung di Website kami. Media belajar kita bersama!</h4>
</body>
</html>
@endsection