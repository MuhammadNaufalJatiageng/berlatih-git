<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Name = ".$sheep->name;
echo "<br>";
echo "Legs = ".$sheep->legs; 
echo "<br>";
echo "Cold blooded = ".$sheep->cold_blooded;

echo "<hr>";

$kodok = new Frog("buduk");
echo "Name = ".$kodok->name;
echo "<br>";
echo "Legs = ".$kodok->legs; 
echo "<br>";
echo "Cold blooded = ".$kodok->cold_blooded;
echo "<br>";
echo "Jump = ".$kodok->jump();

echo "<hr>";

$sungokong = new Ape("kera sakti");
echo "Name = ".$sungokong->name;
echo "<br>";
echo "Legs = ".$sungokong->legs; 
echo "<br>";
echo "Cold blooded = ".$sungokong->cold_blooded;
echo "<br>";
echo "Yell = ".$sungokong->yell();
?>